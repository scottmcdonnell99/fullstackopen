import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Note from './components/Note';
import noteService from './services/notes'
const App = () => {
  const [notes, setNotes] = useState([]);
  const [newNote, setNewNote] = useState('');
  const [showAll, setShowAll] = useState(true);

  // Define a function to fetch notes from the server
  const fetchNotes = () => {
    noteService.getAll().then((data) => {
      setNotes(data);
    }).catch((error) => {
      console.error('Error fetching notes:', error);
    });  
  };

  const toggleImportanceOf = id => {
    const note = notes.find(n => n.id === id)
    const changedNote = { ...note, important: !note.important }
  
    noteService
      .update(id, changedNote).then(returnedNote => {
        setNotes(notes.map(note => note.id !== id ? note : returnedNote))
      })
  
      .catch(error => {
        alert(
          `the note '${note.content}' was already deleted from server`
        )
        setNotes(notes.filter(n => n.id !== id))
      })
  }

  // Use the useEffect hook to fetch notes when the component mounts
  useEffect(() => {
    fetchNotes();
  }, []);

  const notesToShow = showAll
  ? notes
  : notes.filter(note => note.important === true)

  // Function to handle adding a new note
  const addNote = (event) => {
    event.preventDefault();
    const newNoteObject = {
      content: newNote,
      important: Math.random() > 0.5
    };

    noteService.create(newNoteObject).then((data) => {
      setNotes([...notes, data]);
      setNewNote('');
    }).catch((error) => {
      console.error('Error adding a new note:', error);
    });
  };

  return (
    <div>
      <h1>Notes</h1>
      <form onSubmit={addNote}>
        <input
          type="text"
          value={newNote}
          onChange={(e) => setNewNote(e.target.value)}
        />
        <button type="submit">Add Note</button>
      </form>
      <div>
        <button onClick={() => setShowAll(!showAll)}>
          show {showAll ? 'important' : 'all' }
        </button>
      </div>
      <div>
        {notesToShow.map((note) => (
          <Note key={note.id} note={note} toggleImportance={() => toggleImportanceOf(note.id)} 
          />
        ))}
      </div>
    </div>
  );
};

export default App;
