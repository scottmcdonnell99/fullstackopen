const Module = ({ module }) => {
    return (
        <p>{module.name} {module.exercises}</p>
    )
}

const Content = ({ parts }) => {

    const total = parts.reduce((sum, part) => sum + part.exercises, 0);

    return (
        <div>
            {parts.map(module => 
                <Module key={module.id} module={module} />
            )}
            <b> total of {total} exercises</b>
        </div>
    )
}


export default Content