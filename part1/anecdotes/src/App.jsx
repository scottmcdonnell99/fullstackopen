import { useState } from 'react'

const Button = ({ handleClick, text }) => (
    <button onClick={handleClick}>{text}</button>
)

const Header = ({ text }) => (
  <h1>{ text }</h1>
)

const App = () => {
  const anecdotes = [
    'If it hurts, do it more often.',
    'Adding manpower to a late software project makes it later!',
    'The first 90 percent of the code accounts for the first 10 percent of the development time...The remaining 10 percent of the code accounts for the other 90 percent of the development time.',
    'Any fool can write code that a computer can understand. Good programmers write code that humans can understand.',
    'Premature optimization is the root of all evil.',
    'Debugging is twice as hard as writing the code in the first place. Therefore, if you write the code as cleverly as possible, you are, by definition, not smart enough to debug it.',
    'Programming without an extremely heavy use of console.log is same as if a doctor would refuse to use x-rays or blood tests when diagnosing patients.',
    'The only way to go fast, is to go well.'
  ]
   
  const [selected, setSelected] = useState(0)
  const length = anecdotes.length
  const [points, setPoints] = useState(Array(length).fill(0))
  const [max, setMax] = useState(0)

  const handleClick = () =>
  {
    const index = getRandom()
    setSelected(index)
  }

  const handleVoteClick = () =>
  {
    const updatedPoints = [...points]
    updatedPoints[selected] += 1
    setPoints(updatedPoints)

    if (updatedPoints[selected] > updatedPoints[max]) {
      console.log(updatedPoints[selected])
      console.log(updatedPoints[max])
      setMax(selected)
    }

  }


  const getRandom = () => {
    return Math.floor(Math.random() * anecdotes.length)
  }

  return (
    <div>
      <Header text={"Anecdote of the day"} />
      {anecdotes[selected]}
      <div style={{ marginBottom: '5px' }}>
        <p style={{ margin: '0' }}> has {points[selected]} votes</p>
      </div>
      <div>
        <Button handleClick={handleVoteClick} text="vote" />
        <Button handleClick={handleClick} text="next anecdote" />
      </div>
      <Header text={"Anecdote with most votes"} />
      {anecdotes[max]}
      <div style={{ marginBottom: '5px' }}>
        <p style={{ margin: '0' }}> has {points[max]} votes</p>
      </div>
    </div>
  )
}  

export default App