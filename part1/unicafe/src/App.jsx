import { useState } from 'react'

const Header = ({ text }) => ( <h1>{text} </h1> )

const Button = ({ handleClick, text }) => (
  <button onClick={handleClick}>{text}</button>
)

const StatLine = ({ text, value, symbol } ) => ( <tr><td>{text} {value} {symbol}</td></tr>)

const Statistics = ({ good, bad, neutral }) => {
  const total = good + bad + neutral
  const avg = (good - bad) / total
  const positive = (good / total) * 100


  if (total == 0) {
    return (
      <div>
        <p> no feedback given</p>
      </div>
    )
  }

  return (
      <div>
        <table>
          <tbody>
          <StatLine text="good" value={good}/>
          <StatLine text="neutral" value={neutral}/>
          <StatLine text="bad" value={bad}/>
          <StatLine text="total" value={total}/>
          <StatLine text="avg" value={avg}/>
          <StatLine text="positive"  value={positive} symbol={"%"}/>
          </tbody>
        </table>
      </div>
    )
  }

const App = () => {
  const [good, setGood] = useState(0)
  const [neutral, setNeutral] = useState(0)
  const [bad, setBad] = useState(0)


  const handleGoodClick = () => {
    setGood(good + 1)
  }

  const handleNeutralClick = () => {
    setGood(neutral + 1)
  }

  const handleBadClick = () => {
    setGood(bad + 1)
  }


  return (
    <div>
      <Header text="give feedback"/>
      <Button handleClick={handleGoodClick} text="good"/>
      <Button handleClick={handleNeutralClick} text="neutral"/>
      <Button handleClick={handleBadClick} text="bad"/>
      <Header text="statistics"/>
      <Statistics good={good} bad={bad} neutral={neutral} />
    </div>
  )
}

export default App